set autoindent
syntax on
set tabstop=4
set shiftwidth=4
set foldmethod=indent
set foldlevel=99
set expandtab
retab
set number

" Function for inserting the comment markup stuff
"function! NewCMDMarkup()
"    r ~/.vim/NewCMDMarkup.txt
"endfunction
"nmap <C-n> :call NewCMDMarkup()<CR>
